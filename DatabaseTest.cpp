#include <iostream>
#include "Database.h"
#include <time.h>       /* time */

using namespace std;
using namespace Records;


void createDB ( const string& fn, int start, int size ) {
  /* initialize random seed: */
  // srand (time(NULL));

  Database myDB(start); 
  vector<string> firstName = {
    "SMITH", "JOHNSON", "WILLIAMS", "JONES", "BROWN", 
    "DAVIS", "MILLER", "WILSON", "MOORE", "TAYLOR", 
    "ANDERSON", "THOMAS", "JACKSON", "WHITE", "HARRIS", 
    "MARTIN", "THOMPSON", "GARCIA", "MARTINEZ", "ROBINSON"
  };
  vector<string> lastName = {
    "Sophia", "Jackson", "Emma", "Aiden", "Olivia", 
     "Lucas", "Ava Liam", "Mia Noah", "Isabella", "Ethan", 
      "Riley Mason", "Aria", "Caden", "Zoe", "Oliver", 
      "Charlotte", "Elijah", "SMITH", "JOHNSON", "WILLIAMS"
  };

  unsigned n = firstName.size();
  for (int i=0; i < size; ++i ) {
    size_t j = rand()%n;
    size_t k = rand()%n;
    bool hourly = (rand()%3) == 0 ;
    bool fire = (rand()%5) == 0 ;
    bool promote = (rand()%2) == 0 ;
    auto& e = myDB.addEmployee2(firstName[j], lastName[k], hourly);

    size_t m = rand()%10;
    e->setSalary(100000+m*10000);
    if ( promote ) e->promote();
    if (fire) e->fire();
  }
  
  myDB.saveDB ( fn+".txt" );
  myDB.saveBinaryDB( fn+".dat" );
}

int main()
{
  Database myDB;
  createDB ("db1", 400, 200);
  createDB ("db2", 500, 222);

  myDB.readBinaryDB("db1.dat" );
  myDB.readBinaryDB("db2.dat" );

  // myDB.sortDB ( CompareMode::firstName );

  myDB.saveDB ( "db3.txt" );
  myDB.saveBinaryDB( "db3.dat" );
    
	return 0;
}
/*
*/
