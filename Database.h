#pragma once

#include <iostream>
#include <vector>
#include <memory>
#include "Employee.h"

namespace Records {
  using namespace std;
	const int kFirstEmployeeNumber = 1000;

	class Database
	{

	private:
    std::vector<unique_ptr<Employee>> mEmployee;
		std::string  mDatabaseFileName = "db.dat";
		int mNextEmployeeNumber;

	public:
		Database();
		Database(int );
		~Database();

    // copy cstr
    bool addEmployee2( Employee& e) ; 
    unique_ptr<Employee>& addEmployee2(const std::string& firstName,
							  const std::string& lastName, bool hourly = false );

		const Employee* getEmployee(int employeeNumber);
		const auto&     getEmployee(const std::string& firstName, const std::string& lastName);

    // TODO: read DB in the plain text file
		// void readDB (const std::string& fn="db.txt") ;

    // TODO: sort DB with different criteria
    // enum class CompareMode {lastName, firstName, FullName, Salary, id };
    // void sortDB( CompareMode) ;

    // TODO: report the statistics of DB. 
    // Check the given report.txt for details. 
		// void reportDB (const string& fn="report.txt") ;
   

		void displayAll() const;
    void saveDB( const std::string& fn ) const;
		void saveBinaryDB(const std::string& fn="db.dat") const;
		void readBinaryDB (const std::string& fn="db.dat") ;
		void displayCurrent() const;
		void displayFormer() const;
	  bool isDuplicate(int id) const ;
	};
}
