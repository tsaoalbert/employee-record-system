#include <iostream>
#include <iomanip>
#include "Employee.h"

using namespace std;

namespace Records {
// Friend functions are not part of the class and their code must be
// declared outside of the class with no :: Scope Resolution Operator.
// All function arguments must have their class defined

ostream &operator<< (ostream &out, const Employee &e)
{
  int n = 12;
   bool hourly = typeid( e ).name() == string ("N7Records14HourlyEmployeeE");;
   out << right << setw(n ) ;
   out << e.getEmployeeNumber() ;
    out << setw(n ) << e.getLastName() ;
    out << setw(n ) << e.getFirstName() ;
    out << setw(n ) << (e.mHired ? "Current" : "Former") ;
    out << setw(n ) ;
    if ( hourly ) {
      out << "1099" ;
    } else {
      out << "W2" ;
    }
    out << setw(n ) << e.getSalary() ;
    out << endl;
    return out; // This is to keep the stream flowing
}
  SalaryEmployee::SalaryEmployee(std::string f, std::string l, int n )
  :Employee(f,l,n, false)
  {
		 setSalary ( kDefaultStartingSalary );
  }

  // default cstr
	Employee::Employee()
		: mFirstName("")
		, mLastName("")
		, mEmployeeNumber(-1)
		, mSalary(0)
		, mHired(false)
	{
	}

  HourlyEmployee::HourlyEmployee (const std::string& f, const std::string& last, int n, double h, double pr) 
    :   Employee(f,last,n, true) 
  { 
  };

  // copy cstr
  // copy cstr
	Employee::Employee( const Employee& e)
	{
    *this = e ;
	}


/*
	Employee::operator=(const Employee& e) = default ;
	{
	}
*/

	void Employee::promote(int raiseAmount)
	{
		setSalary(getSalary() + raiseAmount);
	}

	void Employee::demote(int demeritAmount)
	{
		setSalary(getSalary() - demeritAmount);
	}

	void Employee::hire()
	{
		mHired = true;
	}

	void Employee::fire()
	{
		mHired = false;
	}

	// Getters and setters
	void Employee::setFirstName(const string& firstName)
	{
		mFirstName = firstName;
	}

	const string& Employee::getFirstName() const
	{
		return mFirstName;
	}

	void Employee::setLastName(const string& lastName)
	{
		mLastName = lastName;
	}

	const string& Employee::getLastName() const
	{
		return mLastName;
	}

	void Employee::setEmployeeNumber(int employeeNumber)
	{
		mEmployeeNumber = employeeNumber;
	}

	int Employee::getEmployeeNumber() const
	{
		return mEmployeeNumber;
	}

	void Employee::setSalary(int salary)
	{
		mSalary = salary;
	}

	int Employee::getSalary() const
	{
		return mSalary;
	}

	bool Employee::getIsHired() const
	{
		return mHired;
	}

    int HourlyEmployee::getSalary() const  /* Compute regHours and overtimeHours */
    {
      int regHours;
      int overtimeHours;
      double regPay;
      double overtimePay;
      double paycheck;

      if (hours <= 40)
      {
        regHours = hours;
        overtimeHours = 0; /* no overtime if less than 40 hours */
      }
      else
      {
        regHours = 40;
        overtimeHours = hours - 40;
      }

      /* Compute the paycheck */
      regPay = regHours * payRate;
      overtimePay = overtimeHours * payRate * 1.5;
      paycheck = regPay + overtimePay;

      return paycheck;
    };
  

}
