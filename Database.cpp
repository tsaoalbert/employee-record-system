#include <iostream>
#include <iomanip>
#include <fstream>
#include <stdexcept>
#include "Database.h"

using namespace std;

namespace Records {

	Database::Database() : mNextEmployeeNumber(kFirstEmployeeNumber)
	{
	}
  Database::~Database(){
  
  for (auto itr = mEmployee.begin(); itr != mEmployee.end(); itr++)
  {
    itr->release();
  }

  }
	Database::Database(int n) : mNextEmployeeNumber(n)
	{
	}

  bool Database::addEmployee2( Employee& e) 
  {
    int n =  e.getEmployeeNumber()  ;
    bool ok =  !isDuplicate( n ) ;
    if ( ok ) {
      mEmployee.emplace_back ( &e ); // xxx
    } else {
      //cout << "Find duplicate employee number: " << n << "!!!" << endl;
    } 
    return ok;
  };

  unique_ptr<Employee>& Database::addEmployee2(
      const string& firstName, const string& lastName, bool hourly )
  {
    int id = mNextEmployeeNumber++ ;
    Employee* e = nullptr ;
    if ( hourly ) {
       e = new HourlyEmployee (firstName, lastName, id, 10, 1.5 );
       mEmployee.emplace_back( e );
    }  else {
       e = new SalaryEmployee (firstName, lastName , id );
       mEmployee.emplace_back( e );
    }
   
    return mEmployee.back();
  }

	const Employee* Database::getEmployee(int employeeNumber)
	{
		for (const auto& e : mEmployee) {
			if (e->getEmployeeNumber() == employeeNumber) {
				return e.get();
			}
		}
		throw runtime_error("No employee found.");
		return nullptr;
	}

	const auto&     Database::getEmployee(const string& firstName, const string& lastName)
	{
		for (auto& e : mEmployee) {
			if (e->getFirstName() == firstName && e->getLastName() == lastName) {
					return e      ;
			}
		}
		throw runtime_error("No employee found.");
	}


  void Database::displayAll() const
  {
    for (const auto& e : mEmployee) {
      cout << *e ;
    }
  }

/*
1000:     Wallis, Greg,   Former,       Wage: $30000
1001:   Gregoire, Marc,  Current,       Wage: $100000
1002:        Doe, John,  Current,     Salary: $11000
*/

  void Database::saveDB( const std::string& fn ) const
  {

    std::filebuf fb;
    fb.open (fn.c_str(),std::ios::out);
    std::ostream os(&fb);
    int n = 12;

    os << "#"  ;
    os << setw(n ) << "Emp No"  ;
    os << setw(n ) << "Last Name"  ;
    os << setw(n ) << "First Name"  ;
    os << setw(n ) << "Status"  ;
    os << setw(n ) << "Type" ;
    os << setw(n ) << "Annual " ;
    os << endl;
    os << "#"  ;
    os << std::string(n*6-1, '-') ;
    os << endl;
    for (const auto& e : mEmployee) {
      // cout << *e ;
      os << *e ;
    }
    fb.close();
  }

	void Database::saveBinaryDB(const std::string& fn) const
	{
    fstream db(fn, ios::out | ios::binary);
    int cnt = mEmployee.size() ;

    db.write(reinterpret_cast<const char *> (&cnt), sizeof(cnt));

    for ( const auto& emp:mEmployee ) 
    {
      Employee* e = emp.get() ;
      bool hourly = typeid(*e) == typeid (HourlyEmployee);

      db.write(reinterpret_cast<const char *> (&hourly), sizeof(hourly));

      if ( hourly ) {
        HourlyEmployee* f=  dynamic_cast<HourlyEmployee* > ( emp.get()) ;
        db.write(reinterpret_cast<const char *> (f), sizeof(*f));
      } else {
        SalaryEmployee* f=  dynamic_cast<SalaryEmployee* > ( emp.get()) ;
        db.write(reinterpret_cast<const char *> (f), sizeof(*f));
      }
		}
    cout << "Save DB: " << cnt << " entries." << endl;
    db.close();
  }

	void Database::readBinaryDB ( const string& fn ) 
	{
    fstream fin ( fn, ios::in | ios::binary);

    if ( !fin ) {
      cout << "Could not find the DB file " << fn << endl;
      return ;
    }
    int cnt = 0 ;
    fin.read(reinterpret_cast<char *> (&cnt), sizeof(cnt));
    cout << "Read DB: file=" <<  fn << ", " << cnt << " entries." << endl;

    int tot = 0 ;
    int dup = 0 ;
    bool ok ;
	  for ( size_t i=0; i < cnt; ++i ) {	
      bool hourly ;
      fin.read(reinterpret_cast<char *> (&hourly), sizeof( hourly ));
	    
      if ( hourly ) {
	      HourlyEmployee *x = new HourlyEmployee() ;
        fin.read(reinterpret_cast<char *> (x), sizeof( *x ));
        ok = addEmployee2( *x); 
      } else {
	      SalaryEmployee *x = new SalaryEmployee() ;
        fin.read(reinterpret_cast<char *> (x), sizeof( *x ));
        ok = addEmployee2( *x); 
      }
      if (ok) tot++;
      if ( !ok ) dup++;
		} 

    fin.close();
    cout << "Add " << tot << " employee's" << endl;
    if (dup>0) {
      cout << "Find " << dup << " duplicate employee numbers!!!" << endl;
    }
	}

	bool Database::isDuplicate(int id) const
	{
		for (const auto& e : mEmployee) {
			if ( e->getEmployeeNumber() == id ) {
        return true;
      }
		}
    return false;
	}

	void Database::displayCurrent() const
	{
		for (const auto& e : mEmployee) {
			if (e->getIsHired())
        cout << *e ;
		}
	}

	void Database::displayFormer() const
	{
		for (const auto& e : mEmployee) {
			if (!e->getIsHired())
        cout << *e ;
		}
	}

}
