#pragma once

#include <string>
#include <iostream>
#include <memory>
#include <fstream>

namespace Records {
  using namespace std;
	const int kDefaultStartingSalary = 30000;

	class Employee
	{
  friend std::ostream &operator<< (std::ostream &out, const Employee &r);

	private:
		std::string mFirstName;
		std::string mLastName;
		int mEmployeeNumber;
		int mSalary;
		bool mHired;
		bool mHourly;

	public:
		Employee();
		Employee(const Employee& );
		Employee(std::string f, std::string l, int n, bool h=true) {
		  mFirstName = f;
		  mLastName = l;
		  mEmployeeNumber = n;
		  mSalary = kDefaultStartingSalary;
		  mHired = 1;
      mHourly = h;
    };
        virtual ~Employee() {} ;
	  Employee& operator= (const Employee& e) = default ;
		virtual void promote(int raiseAmount = 1000);
		virtual void demote(int demeritAmount = 1000);
		void hire(); // Hires or rehires the employee
		void fire(); // Dismisses the employee

		// Getters and setters
		void setFirstName(const std::string& firstName);
		const std::string& getFirstName() const;

		void setLastName(const std::string& lastName);
		const std::string& getLastName() const;

		void setEmployeeNumber(int employeeNumber);
		int getEmployeeNumber() const;

		void setSalary(int newSalary);
		int getSalary() const;
		int getHourly() const { return mHourly; } ;

		bool getIsHired() const;

	};

	class HourlyEmployee: public Employee
	{
    double hours;
    double payRate;
  public:
    // Employee constructor gets 'name' from the Person class
    HourlyEmployee () {};

    HourlyEmployee (const std::string& f, const std::string& last, int n, double h, double pr) ;
    virtual ~HourlyEmployee() override {} ;

   int getSalary() const ; /* Compute regHours and overtimeHours */

  };
  
	class SalaryEmployee: public Employee
	{
  public:
     virtual ~SalaryEmployee() override {} ;
		 int getSalary() const;
     SalaryEmployee() = default;
     SalaryEmployee(std::string f, std::string l, int n );
  };

}
